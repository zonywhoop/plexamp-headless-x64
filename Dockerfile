FROM balenalib/raspberrypi4-64-node:12.22-bullseye-run

RUN apt -y update \
  && apt -y upgrade \
  && apt -y install --no-install-recommends bzip2 alsa-utils pulseaudio pulseaudio-utils
RUN mkdir /app \
  && curl -o /app/plexamp-linux.tbz2 -L https://plexamp.plex.tv/headless/Plexamp-Linux-arm64-v4.2.2.tar.bz2 \
  && tar -jxvf /app/plexamp-linux.tbz2 -C /app \
  && rm -f /app/plexamp-linux.tbz2 
RUN groupadd -g 1001 plexamp \
  && useradd -g 1001 -u 1001 -s /bin/bash -d /home/plexamp -G audio,video,render plexamp \
  && chown -R 1001:1001 /app/plexamp \
  && apt-get -y clean autoclean \
  && apt-get autoremove -y 


COPY confs/asound.conf /etc/asound.conf
COPY confs/pulse-client.conf /etc/pulse/client.conf

USER 1001
WORKDIR /app/plexamp

CMD ["node", "js/index.js"]
